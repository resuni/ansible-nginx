# ansible-nginx

## What this playbook does

- Install Nginx
- Optionally install PHP & PHP-FPM, your choice of version
- Optionally install MariaDB

## What this playbook does NOT do

- Configure firewalls (e.g. firewalld, iptables)
- Configure SSL/HTTPS
- Set the MariaDB root password
- Create MariaDB databases or database users

These tasks need to be performed after the playbook completes, if desired

## Variables

An example inventory file demonstrating the use of these variables is included in this repository.

**php**

Define the version of PHP to be installed, or don't install it at all. 

Applicable values:
- 56
- 70
- 71
- 72
- False


**mariadb**

Enable or disable installation of MariaDB

Applicable values: 
- True
- False

**urls**

An array of URLs to create Nginx config files for (in /etc/nginx/conf.d/).
